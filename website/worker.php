<?php

include_once 'config.php';
include_once 'sql.php';

// Verbingung zu mySQL
// $dbserver, $username, $password, $dbname kommen aus config.php
$conn = connect_to_database($dbserver, $username, $password, $dbname);

//Helper-Funktion, druckt Daten für die Konsole formatiert aus
function print_current_data($timestamp, $temperature, $humidity, $pm10, $pm2p5)
{
    $date = date('Y-m-d H:i:s', $timestamp);
    echo "Zeit: $date\n";
    echo "Temperatur: ${temperature}°C\n";
    echo "rel. Luftfeuchtigkeit: ${humidity}%\n";
    echo "PM10: $pm10 µg/m³\n";
    echo "PM2.5: $pm2p5 µg/m³\n";
}

$timestamp = null;

while (true) {
    // lies Daten von openSenseMap API
    $json = json_decode(file_get_contents("https://api.opensensemap.org/boxes/" . OPENSENSEMAPKEY . "/sensors"), true);
    $pm10 = $json['sensors'][0]['lastMeasurement']['value'];
    $pm2p5 = $json['sensors'][1]['lastMeasurement']['value'];
    $temperature = $json['sensors'][2]['lastMeasurement']['value'];
    $humidity = $json['sensors'][3]['lastMeasurement']['value'];

    // vergleiche alten und neuen Timestamp
    $oldTimestamp = $timestamp;
    $time = $json['sensors'][0]['lastMeasurement']['createdAt'];
    $time = new \DateTime($time);
    $timestamp = $time->getTimestamp();

    // falls es neue Werte gibt:
    if ($timestamp != $oldTimestamp) {
        echo "updated!\n";
        print_current_data($timestamp, $temperature, $humidity, $pm10, $pm2p5, "\n");
        insert_data_set($conn, $table, date('Y-m-d H:i:s', $timestamp), $temperature, $humidity, $pm10, $pm2p5);
        echo "\n";
        // warte bis es neue Werte gibt
    } else {
        echo "wait\n";
        sleep(15);
    }
}

<?php

/**
 * Erstelle eine MySQLi-Connection und gibt diese zurück.
 *
 * Falls ein Fehler passiert, wird dieser ausgedruckt und das weitere Laden der Website gestoppt.
 *
 * @param string $servername mySQL-Hostname
 * @param string $username Benutzername
 * @param string $password Passwort
 * @param string $dbname Name der Datenbank, auf die zugegriffen werden soll.
 */
function connect_to_database($servername, $username, $password, $dbname)
{
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}
/**
 * Schreibt Daten in Datenbank
 *
 * Dafür wird das Tabellellayout benutzt, welches in ../schema.sql definiert ist
 *
 * @param $conn Verbingung, kann z.B. von connect_to_database kommen
 *
 */
function insert_data_set($conn, $table, $time, $temperature, $humidity, $pm10, $pm2p5)
{
    $sqlquery = sprintf("INSERT INTO %s (time, temperature, humidity, pm10, pm2p5) VALUES ('%s', '%s', '%s', '%s', '%s')", $table, $time, $temperature, $humidity, $pm10, $pm2p5);
    exec_sql($conn, $sqlquery);
}

/**
 * Führt eine SQL Anfrage aus.
 *
 * Falls ein Fehler passiert, wird dieser ausgedruckt und das weitere Laden der Website gestoppt.
 *
 * @param $conn Verbindung zu mySQL
 * @param string $sql auszuführende Query
 */
function exec_sql($conn, $sql)
{
    if ($conn->query($sql) !== true) {
        die("Error: " . $conn->error);
    }
    return $conn->insert_id;
}

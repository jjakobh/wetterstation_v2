<?php
include_once "graphs.php";
include_once "sql.php";
include_once "config.php";
?>
<!doctype html>
<html lang="de">

<head>
  <link rel="icon" href="https://lh3.googleusercontent.com/vUVAL5IZJl_9MsS7PQcWotUqinlSEIW_VllIN32y9zZcKH_XVTS1ZtGPgbFRxE42IsSS=w300">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="author" content="Furkan&amp;Johann" />
	<meta name="description" content="Wetterstation - Ein Schulprojekt des CHG" />

  <link rel="stylesheet" href="style.css" />

  <!-- Bootstrap and Dependencies -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
    crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
    crossorigin="anonymous"></script>


  <!-- FusionCharts -->
  <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/3.13.3/fusioncharts.js"></script>
  <script type="text/javascript" src="https://cdn.fusioncharts.com/fusioncharts/3.13.3/themes/fusioncharts.theme.fusion.js"></script>

  <!-- pulltorefresh -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pulltorefreshjs/0.1.16/pulltorefresh.min.js"></script>

  <title>Wetterstation CHG</title>
</head>

<body>

  <script>
    /*PullToRefresh.init({
      mainElement: 'body',
      instructionsPullToRefresh: "yo",
      instructionsRefreshing: "noice",
      instructionsReleaseToRefresh: "refresh",
      onRefresh() { window.location.reload(); },
    });*/
    $(document).ready(function () {
        let searchParams = new URLSearchParams(window.location.search).get("deltatime")
        if(searchParams) {
            zeitraum = $("#zeitraum")
            text = ""
            switch (searchParams) {
                case "-1hour":
                    text = "Eine Stunde"
                    break
                case "-1day":
                    text = "Ein Tag"
                    break
                case "-1year":
                    text = "Ein Jahr"
                    break
            }
            zeitraum.text(zeitraum.text() + ": " + text)
        }


        $.getJSON('https://api.opensensemap.org/boxes/5a699afe411a790019628c13/', function (data) {
          date = new Date(data['lastMeasurementAt'])
          $("#current_values_time").text(date.toLocaleString('de-DE', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
          }));
          let valueList = $("#valueslist")
          for (i = 0; i < data['sensors'].length; i++) {
            info = data['sensors'][i]
            title = info["title"]
            value = info["lastMeasurement"]["value"]
            unit = info["unit"]
            if (title.startsWith("PM")) {
              title = `Feinstaub (${title})`;
            }
            valueList.prepend(`<li class='list-group-item'><p class="identifier">${title}: </p><p class="value">${value}${unit}</p></li>`)
          }
        });
    });
  </script>

<?php
if (!isset($_GET["nonavbar"])) {
    ?>
  <style>
    body {
      padding-top: 65px;
    }
  </style>
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Wetterstation CHG</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="http://carl-humann.de">Carl-Humann Gymnasium <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </nav>
<?php
}
?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-8 col-sm-12">
        <h2>Aktuelle Werte <small id="current_values_time"></small></h2>
        <ul class="list-group" id="valueslist">
          <li class="">
            <div class="dropdown show" width="100%">
              <a id="zeitraum" class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Zeitraum</a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="?deltatime=-1hour">Eine Stunde</a>
                <a class="dropdown-item" href="?deltatime=-1day">Einen Tag</a>
                <a class="dropdown-item" href="?deltatime=-1year">Ein Jahr</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="?">Unbegrenzt</a>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-md-4 col-sm-12">
        <div id="pmgauge"></div>
      </div>
    </div>

    <hr width="100%">

    <div class="row">
      <div class="col-md-6 col-12"> <!-- links -->
        <div class="row">
          <div class="col-md-12">
             <div id="temperature"></div>
          </div>
          <div class="col-md-12">
            <div id="humidity"></div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-12"> <!-- rechts -->
        <div id="pm"></div>
      </div>
    </div>
  </div>

  <?php

if (isset($_GET["deltatime"])) {
    $startTime = date("Y-m-d H:i:s", strtotime($_GET["deltatime"]));
}

// ### Lies Daten aus Datenbank
$conn = connect_to_database($dbserver, $username, $password, $dbname);

$query = "SELECT time, avg(temperature) as temperature, avg(humidity) as humidity, avg(pm10) as pm10, avg(pm2p5) as pm2p5 FROM $table";
if (isset($startTime)) {
    $query .= " WHERE time > '$startTime'";
}
$query .= " GROUP BY UNIX_TIMESTAMP(time) DIV 3600";
$result = $conn->query($query) or exit("Error code ({$conn->errno}): {$conn->error}");

$times = array();
$temperature = array();
$humidity = array();
$pm10 = array();
$pm2p5 = array();

while ($row = mysqli_fetch_array($result)) {
    $datetime = strtotime($row["time"]);
    array_push($times, date("d.m H:i", $datetime));
    array_push($temperature, $row["temperature"]);
    array_push($humidity, $row["humidity"]);
    array_push($pm10, $row["pm10"]);
    array_push($pm2p5, $row["pm2p5"]);
}

// ### zeichne Graphen

$div1_height = 300;
$div2_height = 300;
graph_line("temperature", $times, $temperature, "Temperatur", "°C", "100%", $div1_height);
graph_line("humidity", $times, $humidity, "Luftfeuchtigkeit", "%", "100%", $div2_height);
graph_multiseries("pm", $times, ["PM10", "PM2.5"], [$pm10, $pm2p5], "Feinstaub", "µg/m³", "100%", $div1_height + $div2_height);

// ### zeichne Messgerät
gauge("pmgauge", "Feinstaub Auswertung (PM10)", array_slice($pm10, -1), 0, 75, [
    ["minValue" => "40", "maxValue" => "50", "code" => "#F2726F"],
    ["minValue" => "27", "maxValue" => "40", "code" => "#FFC533"],
    ["minValue" => "0", "maxValue" => "30", "code" => "#62B58F"],
], "100%", "250", array("subCaption" => "in µg/m³"));

?>


</body>

</html>

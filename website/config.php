<?php

// Konstanten
define("FILE", "config.json");
define("OPENSENSEMAPKEY", "5a699afe411a790019628c13");

// lade config aus Konfigurationsdatei
// (u.a. sensitive Daten wie Passwort, welches nicht auf gitlab liegen sollte)
$json = json_decode(file_get_contents(FILE), true);
$username = $json["username"];
$password = $json["password"];
$dbserver = $json["db"]["host"] . ':' . $json["db"]["port"];
$dbname = $json["db"]["database"];
$table = $json["db"]["table"];

// Falls json falsch war, druck und stirb
if (json_last_error() != 0) {
    die(json_last_error_msg());
}

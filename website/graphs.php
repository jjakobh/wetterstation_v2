<?php
include_once "includes/fusioncharts.php";

/**
 * Hilfsfunktion, um arrays für Fusioncharts zu formatieren
 *
 * Beispiel:
 * $key = "kek";
 * $values = [0,1,2];
 *
 * $x = prepend_key($key, $values);
 * // $x ^= [["kek" => 0], ["kek" => 1], ["key" => 2]]
 */
function prepend_key($key, $values)
{
    return array_map(function ($v) use ($key) {
        return array($key => $v);
    }, $values);
}

/**
 * Erstellt Konfigurations-Json-Objekt für Fusioncharts
 */
function graph_config($caption, $chartoptions = array(), $moreoptions = array(), $xaxis_name = "", $yaxis_name = "", $draw_anchors = "0")
{
    return array(
        "chart" => array(
            "caption" => $caption,
            "subcaption" => "in $yaxis_name",
            "drawAnchors" => $draw_anchors,
            "xAxisName" => $xaxis_name,
            //"yAxisName" => $yaxis_name,
            "theme" => "fusion",
        ) + $chartoptions,
    ) + $moreoptions;
}

/**
 * Zeichne Liniengraph.
 *
 * @param $id Die id des divs, welches als Chart gerendert werden soll.
 */
function graph_line($id, $xdata, $ydata, $title, $yaxis, $width, $height, $labelstep = 10, $extraoptions = array())
{
    assert(count($xdata) == count($ydata));

    $step = count($xdata) / $labelstep;

    $arr_config = graph_config(
        $title,
        $extraoptions + array("labelStep" => $step),
        array(),
        "",
        $yaxis
    );

    $to_fusioncharts_format = function ($x, $y) {
        return array(
            "label" => $x,
            "value" => $y,
        );
    };

    $arr_config["data"] = array_map($to_fusioncharts_format, $xdata, $ydata);

    $Chart = new FusionCharts("line", $title, $width, $height, $id, "json", json_encode($arr_config));
    $Chart->render();
}

/**
 * Zeichne Mehrfachliniengraph.
 *
 * @param $id Die id des divs, welches als Chart gerendert werden soll.
 */
function graph_multiseries($id, $xvalues, $titles, $ydatasets, $title, $yaxis, $width, $height, $labelstep = 10, $extraoptions = array())
{
    assert(count($titles) == count($ydatasets));

    $step = count($xvalues) / $labelstep;

    $arr_config = graph_config(
        $title,
        $extraoptions + array("labelStep" => $step),
        array("categories" => array(), "dataset" => array()),
        "",
        $yaxis
    );

    $xcat = prepend_key("label", $xvalues);
    array_push($arr_config["categories"], array(
        "category" => $xcat,
    ));

    $dataset = function ($title, $data) {
        return array(
            "seriesname" => $title,
            "data" => prepend_key("value", $data),
        );
    };
    $arr_config["dataset"] = array_map($dataset, $titles, $ydatasets);

    $Chart = new FusionCharts("msline", $title, $width, $height, $id, "json", json_encode($arr_config));
    $Chart->render();
}

/**
 * Zeichne Messgerät.
 *
 * @param $id Die id des divs, welches als Gauge gerendert werden soll.
 */function gauge($id, $title, $values, $lowlimit, $uplimit, $colorinfo, $width, $height, $extraoptions = array())
{
    $arr_config = array(
        "chart" => array(
            "caption" => $title,
            "lowerLimit" => $lowlimit,
            "upperLimit" => $uplimit,
            "showValue" => "1",
            "theme" => "fusion",
        ) + $extraoptions,
        "colorRange" => array("color" => $colorinfo),
    );

    $dial = prepend_key("value", $values);

    $arr_config["dials"] = array("dial" => $dial);

    $Widget = new FusionCharts("angulargauge", $title, $width, $height, $id, "json", json_encode($arr_config));
    $Widget->render();
}

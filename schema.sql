/* Erstelle Nutzer mit nötigen Rechten */
CREATE USER IF NOT EXISTS 'wetterstation'@'%' IDENTIFIED BY 'changeme';
GRANT SELECT, INSERT, UPDATE, DELETE, FILE ON *.* TO 'wetterstation'@'%';

/* Erstelle Datenbank */
CREATE DATABASE IF NOT EXISTS wetterstation;
CREATE TABLE IF NOT EXISTS wetterstation.sensordaten (
    ID INTEGER PRIMARY KEY AUTO_INCREMENT,
    time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    temperature FLOAT NOT NULL,
    humidity FLOAT NOT NULL,
    pm10 FLOAT NOT NULL,
    pm2p5 FLOAT NOT NULL 
);